# cryptpp
Pure-C++ crypto library

This library is heavily stream-based.

The idea, eventually, is that you will be able to chain operations in a
convenient way (like pipe some input from a file through a sha sum and through
a base64 encoder in one go), though the path to this end isn't yet clear.

Efficiency is also a big thought here, though real optimization is not primary on my mind yet.

There is very much still yet to be done.


This is provided under the permissive MIT license, though I encourage you to
contribute changes back if you make them.  This does mean that you are required
to credit me in software that uses the software (in any sort of documentation
or copyright page does nicely).
