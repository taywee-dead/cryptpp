/*
    Copyright (c) 2015 Taylor C. Richberger

    The contents of this file are licensed MIT.
    The terms of this license are contained in LICENSE.MIT within this repository
*/
#pragma once

#include <iostream>
#include <sstream>
#include <iterator>
#include <functional>

namespace Sha1
{
    static unsigned short BlockSize = 64;

    extern void Sum(std::ostream &output, std::istream &input);

    template <typename InputIterator>
    inline std::string IterSum(InputIterator first, InputIterator last)
    {
        std::stringstream instream;
        std::ostringstream outstream;
        std::copy(first, last, std::ostream_iterator<char>(instream));

        Sum(outstream, instream);
        return outstream.str();
    }
}

extern void Hmac(std::function<void (std::ostream &, std::istream &)> hash, const unsigned int blockSize, std::ostream &output, std::string key, std::istream &message);
