#pragma once

#include <iostream>
#include <sstream>
#include <iterator>

namespace Base32
{
    extern void Decode(std::ostream &output, std::istream &input);

    template <typename InputIterator>
    std::string IterDecode(InputIterator first, InputIterator last)
    {
        std::stringstream instream;
        std::ostringstream outstream;
        std::copy(first, last, std::ostream_iterator<char>(instream));

        Decode(outstream, instream);
        return outstream.str();
    }
}

