#include "coding.hxx"

#include <vector>
#include <iostream>
#include <array>
#include <stdexcept>
#include <string>

namespace Base32
{
    inline unsigned char Reassign(char i)
    {
        switch (i)
        {
            case '0':
                {
                    return static_cast<unsigned char>('O');
                    break;
                }
            case '1':
                {
                    return static_cast<unsigned char>('I');
                    break;
                }
            case '8':
                {
                    return static_cast<unsigned char>('B');
                    break;
                }
            default:
                {
                    if (i >= 'a' && i <= 'z')
                    {
                        return static_cast<unsigned char>(i - 32);
                    } 
                    break;
                }
        }

        return static_cast<unsigned char>(i);
    }

    inline bool checkChar(unsigned char i)
    {
        i = Reassign(i);
        if (i >= 'A' && i <= 'Z')
        {
            return true;
        } else if (i >= '2' && i <= '7')
        {
            return true;
        }

        return false;
    }

    // Do not even need to check padding.  Count is sufficient enough to determine chunk size.
    inline unsigned char Char(char i)
    {
        i = Reassign(i);
        if (i >= 'A' && i <= 'Z')
        {
            return static_cast<unsigned char>(i - 'A');
        } else if (i >= '2' && i <= '7')
        {
            return static_cast<unsigned char>(i - ('2' - 26));
        }
        return 0xFF;
    }

    void Decode(std::ostream &output, std::istream &input)
    {
        /* From RFC 4648:
         *       01234567 89012345 67890123 45678901 23456789
         *      +--------+--------+--------+--------+--------+
         *      |< 1 >< 2| >< 3 ><|.4 >< 5.|>< 6 ><.|7 >< 8 >|
         *      +--------+--------+--------+--------+--------+
         *                                              <===> 8th character
         *                                        <====> 7th character
         *                                   <===> 6th character
         *                             <====> 5th character
         *                       <====> 4th character
         *                  <===> 3rd character
         *            <====> 2nd character
         *       <===> 1st character
         */


        while (input)
        {
            std::vector<char> chunk;
            chunk.reserve(8);

            while (chunk.size() < 8)
            {
                int in = input.get();

                if (in == std::char_traits<char>::eof())
                {
                    break;
                }
                if (checkChar(in))
                {
                    chunk.push_back(static_cast<char>(in));
                }
            }

            std::array<char, 5> outChunk;

            for (char &c: outChunk)
            {
                c = 0;
            }

            unsigned char piece;

            switch (chunk.size())
            {
                // If there are no characters left to consume, exit
                case 0:
                    {
                        return;
                        break;
                    }

                case 8:
                    {
                        piece = Char(chunk[7]);
                        outChunk[4] |= piece;
                    }

                case 7:
                    {
                        piece = Char(chunk[6]);
                        outChunk[3] |= piece >> 3;
                        outChunk[4] |= piece << 5;
                    }

                case 6:
                    {
                        piece = Char(chunk[5]);
                        outChunk[3] |= piece << 2;
                    }

                case 5:
                    {
                        piece = Char(chunk[4]);
                        outChunk[2] |= piece >> 1;
                        outChunk[3] |= piece << 7;
                    }

                case 4:
                    {
                        piece = Char(chunk[3]);
                        outChunk[1] |= piece >> 4;
                        outChunk[2] |= piece << 4;
                    }

                case 3:
                    {
                        piece = Char(chunk[2]);
                        outChunk[1] |= piece << 1;
                    }

                case 2:
                    {
                        piece = Char(chunk[1]);
                        outChunk[0] |= piece >> 2;
                        outChunk[1] |= piece << 6;
                    }

                case 1:
                    {
                        piece = Char(chunk[0]);
                        outChunk[0] |= piece << 3;
                    }

                default:
                    {
                        break;
                    }
            }

            unsigned char outputSize = 0;

            switch (chunk.size())
            {
                case 1:
                    {
                        outputSize = 1;
                        break;
                    }

                case 2:
                case 3:
                    {
                        outputSize = 2;
                        break;
                    }

                case 4:
                    {
                        outputSize = 3;
                        break;
                    }

                case 5:
                case 6:
                    {
                        outputSize = 4;
                        break;
                    }

                default:
                    {
                        outputSize = 5;
                        break;
                    }
            }
            output.write(outChunk.data(), outputSize);
        }
    }
}

