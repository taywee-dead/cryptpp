#include "crypto.hxx"

#include <iterator>
#include <iostream>
#include <algorithm>
#include <cstdint>
#include <array>
#include <vector>

// Rotate a 32-bit int left by a places count
inline uint32_t LRot32(const uint32_t number, uint8_t places)
{
    places = places % 32;
    if (places != 0)
    {
        return (number << places) | (number >> (32 - places));
    } else
    {
        return number;
    }
}

// Template overload of the same for better speed, with no size checking
template <uint8_t places>
inline uint32_t LRot32(const uint32_t number)
{
    constexpr uint8_t rplaces = 32 - places;

    return (number << places) | (number >> rplaces);
}

template <>
inline uint32_t LRot32<0>(const uint32_t number)
{
    return number;
}

// Pack 4 8-bit ints into a 32-bit int.  Must use either bit-shifting or know endianness.
inline uint32_t Pack32(const uint8_t a, const uint8_t b, const uint8_t c, const uint8_t d)
{
    return (static_cast<uint32_t>(a) << 24)
            | (static_cast<uint32_t>(b) << 16)
            | (static_cast<uint32_t>(c) << 8)
            | static_cast<uint32_t>(d);
}

// Splits 32-bit int into 4 8-bit ints.
inline void Unpack32(const uint32_t input, uint8_t &a, uint8_t &b, uint8_t &c, uint8_t &d)
{
    a = static_cast<uint8_t>(input >> 24);
    b = static_cast<uint8_t>(input >> 16);
    c = static_cast<uint8_t>(input >> 8);
    d = static_cast<uint8_t>(input);
}

namespace Sha1
{
    // Sha1 special functions
    inline uint32_t F1(const uint32_t a, const uint32_t b, const uint32_t c)
    {
        return (a & b) | ((~a) & c);
    }

    inline uint32_t F2(const uint32_t a, const uint32_t b, const uint32_t c)
    {
        return a ^ b ^ c;
    }

    inline uint32_t F3(const uint32_t a, const uint32_t b, const uint32_t c)
    {
        return (a & b) | (a & c) | (b & c);
    }

    inline void LoopFunction(const std::array<char, 64> &chunk, std::array<uint32_t, 5> &h)
    {
        std::array<uint32_t, 80> words;

        for (unsigned int i = 0; i < 16; ++i)
        {
            unsigned int startChunk = 4 * i;

            words[i] = Pack32(chunk[startChunk], chunk[startChunk + 1], chunk[startChunk + 2], chunk[startChunk + 3]);
        }


        // Do some weird calculations to fill up to 80 pieces
        for (unsigned int i = 16; i < 80; ++i)
        {
            words[i] = words[i - 3] ^ words[i - 8] ^ words[i - 14] ^ words[i - 16];
            words[i] = LRot32<1>(words[i]);
        }

        uint32_t a = h[0];
        uint32_t b = h[1];
        uint32_t c = h[2];
        uint32_t d = h[3];
        uint32_t e = h[4];
        uint32_t f;
        uint32_t k;

        for (unsigned int i = 0; i < 80; ++i)
        {
            if (i < 20)
            {
                f = F1(b, c, d);
                k = 0x5A827999u;
            } else if (i < 40)
            {
                f = F2(b, c, d);
                k = 0x6ED9EBA1u;
            } else if (i < 60)
            {
                f = F3(b, c, d);
                k = 0x8F1BBCDCu;
            } else
            {
                f = F2(b, c, d);
                k = 0xCA62C1D6u;
            }
            const uint32_t temp = LRot32<5>(a) + f + e + k + words[i];
            e = d;
            d = c;
            c = LRot32<30>(b);
            b = a;
            a = temp;
        }
        h[0] += a;
        h[1] += b;
        h[2] += c;
        h[3] += d;
        h[4] += e;
    }

    void Sum(std::ostream &output, std::istream &input)
    {
        std::array<uint32_t, 5> h;
        h[0] = 0x67452301u;
        h[1] = 0xEFCDAB89u;
        h[2] = 0x98BADCFEu;
        h[3] = 0x10325476u;
        h[4] = 0xC3D2E1F0u;

        uint64_t size = 0;

        while (input)
        {
            std::array<char, 64> chunk;
            
            input.read(chunk.data(), 64);

            if (input)
            {
                size += (64 * 8);
            } else
            {
                const uint64_t count = input.gcount();
                size += (count * 8);

                // If input is bad, the count is always less than 64.
                if (count > 55)
                {
                    chunk[count] = 0x80;
                    for (unsigned char i = count + 1; i < 64; ++i)
                    {
                        chunk[i] = 0x00;
                    }

                    // Because the count isn't congruent to 448 bits (56 bytes),
                    // We need to run the loop function twice, so that there is
                    // room for the message size on the end.
                    LoopFunction(chunk, h);

                    for (unsigned char i = 0; i < 56; ++i)
                    {
                        chunk[i] = 0x00;
                    }
                } else
                {
                    chunk[count] = 0x80;
                    for (unsigned char i = count + 1; i < 56; ++i)
                    {
                        chunk[i] = 0x00;
                    }
                }

                // Append the size to the end
                for (unsigned char i = 0; i < 8; ++i)
                {
                    chunk[i + 56] = static_cast<uint8_t>(size >> ((7 - i) * 8));
                }
            }

            LoopFunction(chunk, h);
        }

        for (unsigned int i = 0; i < 5; ++i)
        {
            std::array<unsigned char, 4> out;
            Unpack32(h[i], out[0], out[1], out[2], out[3]);
            output.write(reinterpret_cast<char *>(out.data()), 4);
        }
    }
}

void Hmac(std::function<void (std::ostream &, std::istream &)> hash, const unsigned int blockSize, std::ostream &output, std::string key, std::istream &message)
{
    if (key.size() > blockSize)
    {
        std::istringstream oldKey(key);
        std::ostringstream newKey;
        hash(newKey, oldKey);
        key.assign(newKey.str());
    }
    if (key.size() < blockSize)
    {
        key.resize(blockSize, '\0');
    }

    std::vector<char> o_key_pad(blockSize);
    std::vector<char> i_key_pad(blockSize);

    for (unsigned int i = 0; i < blockSize; ++i)
    {
        o_key_pad[i] = 0x5c ^ key[i];
        i_key_pad[i] = 0x36 ^ key[i];
    }

    std::stringstream insideMessage;
    insideMessage.write(i_key_pad.data(), i_key_pad.size());
    insideMessage << message.rdbuf();

    std::stringstream hashedInside;
    hash(hashedInside, insideMessage);

    std::stringstream outsideMessage;
    outsideMessage.write(o_key_pad.data(), o_key_pad.size());
    outsideMessage << hashedInside.rdbuf();

    hash(output, outsideMessage);
}
