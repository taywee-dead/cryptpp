build = Release
objects = coding.o crypto.o
CC = clang
CXX = clang++
AR = ar

commonOptsAll = -Wall -Wextra -std=c++11 -fPIC
commonDebugOpts = -ggdb -O0 -DDEBUG
commonReleaseOpts = -O3 -march=native
commonOpts = $(commonOptsAll) $(common$(build)Opts)

compileOptsAll = -c
compileOptsRelease =
compileOptsDebug =
compileOpts = $(commonOpts) $(compileOptsAll) $(compileOpts$(build))

compile = $(CXX) $(compileOpts)

.PHONY : all clean

all : libcryptpp.a 

clean :
	-rm -v libcryptpp.a $(objects)

libcryptpp.a : $(objects)
	$(AR) rcs $@ $^

coding.o : coding.cxx coding.hxx
	$(compile) -o coding.o coding.cxx

crypto.o : crypto.cxx crypto.hxx
	$(compile) -o crypto.o crypto.cxx
